class Environment:
    """
    Get some attributes from prisma environment files
    """

    def __init__(self, env_file):
        prisma_env = open(env_file, 'r')
        self.lines = prisma_env.readlines()
        prisma_env.close()

    @property
    def prisma_secret(self):
        found = list(filter((lambda line: 'PRISMA_SECRET' in line), self.lines))
        if len(found) == 1:
            return found[0].strip().split('=')[1].strip('"')

    @property
    def service(self):
        found = list(filter((lambda line: 'PRISMA_ENDPOINT' in line), self.lines))
        if len(found) == 1:
            return '@'.join(found[0].strip().split('=')[1].strip('"').split('/')[-2:])

