import os
import os.path
import subprocess as sp
import json

from jose import jwt


from gqlc import GraphQLClient
from spectrum.prisma_env import Environment


import errno
import configparser

CONFIG_LOCATION = os.path.join(os.environ['HOME'], '.config/spectrum')
CONFIG_FILE = 'spectrum.cfg'
CONFIG_PATH = os.path.join(CONFIG_LOCATION, CONFIG_FILE)


config = configparser.ConfigParser()

if not os.path.exists(os.path.dirname(CONFIG_PATH)):
    print('CONFIG NOT FOUND')
    try:
        os.makedirs(os.path.dirname(CONFIG_PATH))
    except OSError as exc:  # Guard against race condition
        if exc.errno != errno.EEXIST:
            raise

    config['PROJECT'] = {'HOME': os.path.join(os.environ['HOME'], 'project')}
    config['LOCATION'] = {'ENVS': '__envs__', 'DATA': '__data__'}

    with open(CONFIG_PATH, "w") as fp:
        config.write(fp)

config.read(CONFIG_PATH)


PROJECT_HOME = config['PROJECT']['HOME']
ENVS = config['LOCATION']['ENVS']
DATA = config['LOCATION']['DATA']


class Client:
    """
    Wrapper around Prisma to do some chores
    - save_state('name'): exports data to a given zip file => __data_/name.zip
    - set_state('name): imports date from __data__/name.zip to current instance
      - [DESTRUCTIVE] will reset current instance before import
    """

    def __init__(self, instance):
        self.instance = instance

        self.context = GraphQLClient(self.endpoint)
        self.context.inject_token(self.token())
        self.context.app = GraphQLClient(self.gql_server)

    def token(self, claims=None):
        if claims:
            return self.mint(claims)
        token = sp.check_output(['prisma', 'token', '--env-file', os.path.join(ENVS, self.instance)], cwd=PROJECT_HOME)
        return token.decode('utf8').strip()

    def mint(self, claims):
        """
        Mint own token
        """
        env_file = os.path.join(PROJECT_HOME, os.path.join(ENVS, self.instance))
        environment = Environment(env_file)
        payload = {
            "data": {
                "service": environment.service,
                "roles": ["admin"]
            }
        }
        payload.update(claims)
        jwt_string = jwt.encode(payload, environment.prisma_secret, algorithm='HS256')
        return jwt_string

    @property
    def endpoint(self):
        info = sp.check_output(['prisma', 'info', '--json', '--env-file', os.path.join(ENVS, self.instance)],
                               cwd=PROJECT_HOME)
        return json.loads(info.decode('utf8'))['httpEndpoint']

    @property
    def gql_server(self):
        filename = 'gqls.' + self.instance
        filepath = os.path.join(os.path.join(PROJECT_HOME, ENVS), filename)
        config.read(filepath)
        return config['SERVER']['URL']

    def reset(self):
        sp.check_output(['prisma', 'reset', '--force', '--env-file', os.path.join(ENVS, self.instance)],
                        cwd=PROJECT_HOME)

    def load(self, state):
        sp.check_output(['prisma', 'import', '--env-file', os.path.join(ENVS, self.instance), '--data',
                         os.path.join(DATA, state + '.zip')], cwd=PROJECT_HOME)

    def deploy(self):
        info = sp.check_output(['prisma', 'deploy', '--env-file', os.path.join(ENVS, self.instance)], cwd=PROJECT_HOME)
        return info

    def set_state(self, state):
        self.reset()
        self.load(state)

    def save_state(self, state):
        sp.check_output(['prisma', 'export', '--env-file', os.path.join(ENVS, self.instance), '--path',
                         os.path.join(DATA, state + '.zip')], cwd=PROJECT_HOME)

    def execute(self, query):
        return self.context.execute(query)
