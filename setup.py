from setuptools import setup

setup(
    name='spectrum',
    version='0.0.1',
    packages=['spectrum'],
    url='https://bitbucket.org/koodir/spectrum',
    license='MIT',
    author='koodir',
    author_email='koodir@gmx.com',
    description='Prisma wrapper/graphql-client',
    install_requires=[
        'graphqlclient==0.0.1',
        'certifi==2017.4.17',
        'chardet==3.0.4',
        'ecdsa==0.13',
        'future==0.16.0',
        'idna==2.5',
        'pyasn1==0.4.4',
        'python-jose==3.0.0',
        'requests==2.18.1',
        'rsa==3.4.2',
        'six==1.11.0',
        'urllib3==1.21.1'
    ],
    dependency_links=['https://bitbucket.org/koodir/graphqlclient/get/5bec78efffa3.zip#egg=graphqlclient-0.0.1']
)
